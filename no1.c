/*
 ARYADI YOGI MEZZOVANI
 302230021
 */

 // Deklarasi fungsi dan prosedur
void inputNilai(double *quiz, double *absen, double *uts, double *uas, double *tugas);
double hitungNilai(double quiz, double absen, double uts, double uas, double tugas);  
void cetakNilai(double quiz, double absen, double uts, double uas, double tugas);
void cetakMutu(double nilai, char hurufMutu);

int main() {

  double quiz, absen, uts, uas, tugas, nilai;
  char hurufMutu;

  inputNilai(&quiz, &absen, &uts, &uas, &tugas);

  cetakNilai(quiz, absen, uts, uas, tugas);

  nilai = hitungNilai(quiz, absen, uts, uas, tugas);

  cetakMutu(nilai, hurufMutu);

  return 0;  
}

// Fungsi input nilai
void inputNilai(double *quiz, double *absen, double *uts, double *uas, double *tugas) {

  *quiz = 40; 
  *absen = 100;
  *uts = 60;
  *uas = 50;
  *tugas = 80;
  
}

// Fungsi hitung nilai
double hitungNilai(double quiz, double absen, double uts, double uas, double tugas) {

  double nilai;

  nilai = ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;

  return nilai;

}

// Prosedur cetak nilai
void cetakNilai(double quiz, double absen, double uts, double uas, double tugas) {

  printf("Absen = %.2lf UTS = %.2lf\n", absen, uts);
  printf("Tugas = %.2lf UAS = %.2lf\n", tugas, uas);
  printf("Quiz = %.2lf\n", quiz);
  
}

// Prosedur cetak mutu 
void cetakMutu(double nilai, char hurufMutu) {

  if (nilai > 85 && nilai <= 100)
    hurufMutu = 'A';
  else if (nilai > 70 && nilai <= 85)
    hurufMutu = 'B';
  else if (nilai > 55 && nilai <= 70)
    hurufMutu = 'C';
  else if (nilai > 40 && nilai <= 55)
    hurufMutu = 'D';
  else if (nilai >= 0 && nilai <= 40)
    hurufMutu = 'E'; 

  printf("Huruf Mutu: %c\n", hurufMutu);

}
